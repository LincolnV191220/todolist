import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/model/task';
import { CrudService } from 'src/app/service/crud.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  taskOjb: Task = new Task();
  taskArr: Task[] = [];

  addTaskValue: string = '';
  editTaskValue: string = '';

  constructor(private crudService: CrudService) {

  }

  ngOnInit(): void {
    this.taskOjb = new Task();
    this.editTaskValue = '';
    this.addTaskValue = '';
    this.taskArr = [];
    this.getAllTask();
  }

  getAllTask() {
    this.crudService.getAllTask().subscribe(res => {
      this.taskArr = res;
    }, err => {
      alert("Unable to get list of tasks");
    })
  }

  addTask() {
    this.taskOjb.task_name = this.addTaskValue
    this.crudService.addTask(this.taskOjb).subscribe(res => {
      this.ngOnInit();
      this.addTaskValue = '';
    }, err => {
      alert(err);
    })
  }

  editTask() {
    this.taskOjb.task_name = this.editTaskValue;
    this.crudService.editTask(this.taskOjb).subscribe(res => {
      this.ngOnInit;
    }, err => {
      alert('Fail to update task');
    })
  }

  deleteTask(etask: Task) {
    this.crudService.deleteTask(etask).subscribe(res => {
      this.ngOnInit();
    }, err => {
      alert("Failed to delete task");
    });
  }

  call(etask: Task) {
    this.taskOjb = etask;
    this.editTaskValue = etask.task_name;
  }
}
